import LoadFonts from './modules/LoadFonts';
import { $, $$ } from './modules/Bling';
import { url, sayHi } from './modules/temp';
import Dog from './modules/Dog';

LoadFonts();

const snickers = new Dog('Snickers', 'King Charles');
const sunny = new Dog('Sunny', 'Golden Doodle');

console.log(snickers);
console.log(sunny);
console.log(url);
sayHi('test');

// add click evt to body and log target el
$('body').on('click', el => console.log(el.target));

// loop over all paragph elements and log
$$('p').forEach(el => console.log(el));

console.log('test');
