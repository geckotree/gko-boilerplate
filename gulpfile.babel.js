// import path from 'path';
import del from 'del';
import gulp from 'gulp';
import { rollup } from 'rollup';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';
import fileinclude from 'gulp-file-include';
import gulpLoadPlugins from 'gulp-load-plugins';
import CacheBuster from 'gulp-cachebust';

const $ = gulpLoadPlugins();
const cachebust = new CacheBuster();
let isProduction = false;

const paths = {
	assetsFolder: '_src',
	templates: '_src/views',
	siteFolder: 'gko-boilerplate.web',
	assetsBuildFolder: 'gko-boilerplate.web',
	masterTemplate: '_templates/Master.cshtml'
};

// Lint JavaScript
gulp.task('lint', () =>
  gulp.src([`${paths.assetsFolder}/js/**/*.js`, `!${paths.assetsFolder}/js/lib/*.js`, '!node_modules/**'])
    .pipe($.eslint())
    .pipe($.eslint.format())
);

// CSS
gulp.task('css', () => {
	const AUTOPREFIXER_BROWSERS = [
		'ie >= 10',
		'ie_mob >= 10',
		'ff >= 30',
		'chrome >= 34',
		'safari >= 7',
		'opera >= 23',
		'ios >= 7',
		'android >= 4.4',
		'bb >= 10'
	];

	gulp.src(`${paths.assetsFolder}/sass/*.scss`)
	.pipe($.newer('.tmp/styles'))
	.pipe($.sourcemaps.init())
	.pipe($.sass({
		precision: 10
	}).on('error', $.sass.logError))
	.pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
	.pipe(gulp.dest('.tmp/styles'))
	.pipe($.if(isProduction, $.combineMq({ beautify: false })))
	.pipe($.if(isProduction, $.cssnano()))
	.pipe($.sourcemaps.write('./'))
	.pipe($.if(isProduction, cachebust.resources()))
	.pipe(gulp.dest(`${paths.assetsBuildFolder}/css`));
});

// JS
gulp.task('js', () => rollup({
	entry: `${paths.assetsFolder}/js/main.js`,
	plugins: [
		nodeResolve({
			jsnext: true,
			main: true
		}),
		commonjs({
			// non-CommonJS modules will be ignored, but you can also
			// specifically include/exclude files
			include: 'node_modules/**',  // Default: undefined
			//  exclude: ['node_modules/foo/**', 'node_modules/bar/**'],  // Default: undefined
			extensions: ['.js'],
			ignoreGlobal: false,
			sourceMap: true,
			// explicitly specify unresolvable named exports
			//namedExports: { './module.js': ['foo', 'bar' ] }  // Default: undefined
		}),
		babel({
			exclude: 'node_modules/**'
		})
	],
})
.then((bundle) => {
	bundle.write({
		format: 'es',
		moduleName: 'gko',
		dest: `${paths.assetsBuildFolder}/js/main.js`,
		sourceMap: true
	});
}));

// Modernizr
gulp.task('modernizr', () => {
	gulp.src([
		`!${paths.assetsFolder}/js/lib/modernizr.js`,
		`${paths.assetsFolder}/js/**/*.js`,
		`${paths.assetsFolder}'/sass/**/*.scss`
	])
	.pipe($.modernizr({
		options: ['setClasses']
	}))
	.pipe(gulp.dest(`${paths.assetsFolder}/js/lib`));
});

// Optimize images
gulp.task('images', () =>
	gulp.src(`${paths.assetsFolder}/images/**/*`)
    .pipe($.cache(
		$.imagemin({
			progressive: true,
			interlaced: true
		})
	))
    .pipe(gulp.dest(`${paths.assetsBuildFolder}/images`))
    .pipe($.size({ title: 'images' }))
);

// Copy fonts
gulp.task('copyfonts', () => {
	gulp.src(`${paths.assetsFolder}/fonts/**/*`)
	.pipe($.newer('.tmp/fonts'))
	.pipe(gulp.dest('.tmp/fonts'))
	.pipe(gulp.dest(`${paths.assetsBuildFolder}/fonts`));
});

// Clear the image cache to force reoptims
gulp.task('clearCache', done => $.cache.clearAll(done));

// Clean output directory
gulp.task('clean', () => del([
	'.tmp',
	`${paths.assetsBuildFolder}/css`,
	`${paths.assetsBuildFolder}/js`,
	`${paths.assetsBuildFolder}/img`,
	`${paths.assetsBuildFolder}/fonts`
]));


// file includes
gulp.task('templates', () => {
	gulp.src(`${paths.templates}/*.html`)
    .pipe(fileinclude({
	prefix: '@',
	basepath: '@file'
}))
    .pipe(gulp.dest(paths.siteFolder));
});

// Watch task
gulp.task('watch', () => {
	gulp.watch(`${paths.assetsFolder}/sass/**/*.scss`, ['css']);
	gulp.watch(`${paths.assetsFolder}/js/**/*.js`, ['js']);
	gulp.watch(`${paths.assetsFolder}/images/**/*`, ['images']);
	gulp.watch(`${paths.templates}/**/*`, ['templates']);
});

// Copy master template with correct asset references
gulp.task('refAssets', ['css', 'js'], () => {
	gulp.src(paths.masterTemplate)
		.pipe($.if(isProduction, cachebust.references()))
        .pipe(gulp.dest(`${paths.siteFolder}/Views`));
});

// gulp dev
gulp.task('dev', ['clean', 'modernizr'], () => {
	isProduction = false;
	gulp.start('refAssets', 'images', 'watch', 'copyfonts');
});


// gulp build
gulp.task('build', ['clean'], () => {
	isProduction = true;
	gulp.start('refAssets', 'images', 'copyfonts');
});
